# reminder

Let's you create simple periodical reminders for yourself. **This application is windows only**

## How to build

`cargo build -r`

## How to use

Just execute the `.exe`.

## How to configure

Create a directory `.reminder` in `%USERPROFILE%`.

In the directory create a file `config.ini`.

Create a section with an arbitrary name for each notification and set the three self-explaining parameters `caption`, `text`, `seconds`.

### Example

```ini
[Water]
caption=STAY HYDRATED!
text=Refresh yourself with some water
seconds=1200
```

This example would notify you every 20 minutes to drink some water.

use std::fmt::Debug;

use configparser::ini::Ini;

use crate::reminder_job::ReminderJob;

#[derive(Debug)]
pub struct Config {
    file_path: String,
}

impl Config {
    pub fn new(file_path: &str) -> Self {
        return Config {
            file_path: String::from(file_path),
        };
    }

    pub fn get_jobs(&self) -> Vec<ReminderJob> {
        let mut jobs = vec![];
        let cfg = Ini::new()
            .load(&self.file_path)
            .expect(format!("Couldn't load config from <{}>", &self.file_path).as_str());
        dbg!(&cfg);
        for (_outer_key, inner_map) in &cfg {
            let caption = inner_map.get("caption").unwrap().clone().unwrap();
            let text = inner_map.get("text").unwrap().clone().unwrap();
            let seconds = inner_map
                .get("seconds")
                .unwrap()
                .clone()
                .unwrap()
                .parse::<u32>()
                .unwrap();
            let job = ReminderJob::new(&caption, &text, seconds);
            jobs.push(job);
        }
        return jobs;
    }
}

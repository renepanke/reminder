pub mod config;
mod reminder_job;

use std::{path::Path, thread, time::Duration};

use clokwerk::{Scheduler, TimeUnits};
use config::Config;
use dirs;

fn main() {
    let config_file_path_str = dirs::home_dir()
        .expect("Couldn't determine home directory!")
        .join(Path::new(".reminder"))
        .join("config.ini");

    let config = Config::new(
        &config_file_path_str
            .as_path()
            .to_str()
            .expect("Couldn't get file on %USERPROFILE%\\.reminder\\config.ini"),
    );
    dbg!(&config.get_jobs());

    let mut scheduler = Scheduler::new();

    for job in config.get_jobs() {
        scheduler
            .every(job.seconds.seconds())
            .run(move || job.notify());
    }

    loop {
        scheduler.run_pending();
        thread::sleep(Duration::from_millis(100));
    }
}

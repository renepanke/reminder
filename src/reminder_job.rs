use chrono::Local;
use winrt_notification::{Duration, Toast};

#[derive(Debug)]
pub struct ReminderJob {
    caption: String,
    text: String,
    pub seconds: u32,
}

impl ReminderJob {
    pub fn new(caption: &str, text: &str, seconds: u32) -> Self {
        return ReminderJob {
            caption: String::from(caption),
            text: String::from(text),
            seconds: seconds,
        };
    }

    pub fn notify(&self) {
        println!(
            "Running ReminderJob <{}> at {:?}",
            self.caption,
            Local::now()
        );
        Toast::new(Toast::POWERSHELL_APP_ID)
            .title(self.text.as_str())
            .text1(self.caption.as_str())
            .duration(Duration::Short)
            .show()
            .expect("Couldn't display notification!");
    }
}
